HipChat Timely
==============

This addon shows timezones for all participants in a room.

![alt text](./docs/screen-shot-timely.png)

It also shows how easy it is to get up and running with a new addon to HipChat.
