import asyncio
import logging
import pytz

from datetime import datetime

from aio_hipchat.hc_client import HcClient
from aio_hipchat.toolkit import MillisecondTimer

_logger = logging.getLogger(__name__)


class TimezoneController:
    """
    This class does the actual work of calculating timezones for a given room
    """

    def __init__(self, oauth):
        self.oauth = oauth

    @asyncio.coroutine
    def room_members(self):
        """
        Return the members of the room that should be considered when calculating timezones
        """
        hc_client = HcClient(self.oauth)
        participants = yield from hc_client.get_participants(self.oauth.room_id)

        return participants

    @asyncio.coroutine
    def get_timezone_buckets(self):
        """
        Calculate the timezones currently in use in the room, as well as useful details about
        those timezones.
        """

        _logger.debug("Calculating timezones for room %d...", self.oauth.room_id)

        timer = MillisecondTimer()
        members = yield from self.room_members()

        # Order members by their last activity time
        members = sorted(members, key=lambda x: x["last_active"], reverse=True)

        # Group room members by their timezone
        members_by_timezone = {}
        for x in members:
            members_by_timezone.setdefault(x['timezone'], list()).append(x)

        now = datetime.now()

        def get_utc_offset(tz):
            # UTC offset in the form +HHMM or -HHMM (empty string if the the object is naive).
            offset = tz.localize(now).strftime('%z')
            return offset[:3] + "." + offset[3:] if offset else "0"

        def get_better_timezone_name(tz):
            name = tz.zone.split("/")[-1]
            return name.replace("_", " ")

        def make_timezone_bucket(tz_name, members_in_timezone):
            timezone = pytz.timezone(tz_name)
            bucket = {
                "name": get_better_timezone_name(timezone),
                "utc-offset": get_utc_offset(timezone),
                "tz-name": tz_name,
                "tz": timezone,
                "users": members_in_timezone,
                "user-count": len(members_in_timezone)
            }
            return bucket

        timezones = [make_timezone_bucket(tz_name, members) for (tz_name, members) in members_by_timezone.items()]

        _logger.debug("Calculated timezones for room %d (in %d ms): %r",
                      self.oauth.room_id, timer.end(), timezones)

        return timezones
