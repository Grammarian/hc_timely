(function () {

    // not currently used

    function initWebSocket(baseUrl) {
        console.log("initWebSocket: " + baseUrl);

        var uri = new URI(baseUrl);
        var protocol = uri.protocol() === "https" ? "wss://" : "ws://";
        var port = uri.port() ? (":" + uri.port()) : "";
        var ws_url = protocol + uri.hostname() + port + "/websocket";
        var socket = new WebSocket(ws_url);

        socket.onmessage = function (event) {
            var message = JSON.parse(event.data);
            var existing = $("summary-count");
            existing.replaceWith(message.html);
        };
    }

    $(document).ready(function () {

        var baseUrl = $("meta[name=base-url]").attr("content");

        initWebSocket(baseUrl);
    });
})();
