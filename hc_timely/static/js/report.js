(function () {

    // Listen for incoming updates from the server (via SSE) and display
    // the received HTML in the sidebar container element

    $(document).ready(function () {
        console.log('ready');

        var $spinner = $("#spinner-container");
        $spinner.spin("medium");

        if (typeof(EventSource) == "undefined") {
            console.log('ERROR: Server-sent events not supported')
            $spinner.spin(false);
            document.getElementById("errors-container").innerHTML = "<h3>This browser does not support server side events</h3>";
        } else {
            var baseUrl = $("meta[name=base-url]").attr("content");
            var evtSrc = new EventSource(baseUrl + "/subscribe");

            evtSrc.onmessage = function (e) {
                var obj = JSON.parse(e.data);
                console.log(obj);

                if ($spinner !== null) {
                    $spinner.spin(false);
                    $spinner = null;
                }

                var timezonesHtmlContainer = document.getElementById("timezone-container");
                timezonesHtmlContainer.innerHTML = obj.html;
                CoolClock.findAndCreateClocks();
            };
            evtSrc.onerror = function (e) {
                var $errorMsg = "";
                if (e.readyState == EventSource.CLOSED) {
                    $errorMsg = "SSE networkError";
                }
                else if (e.readyState == EventSource.OPEN) {
                    $errorMsg = "SSE connecting";
                } else {
                    $errorMsg = 'SSE ERROR:' + e.data;
                }
                console.log($errorMsg);
                document.getElementById("errors-container").innerHTML = '<p>ERROR:' + $errorMsg + '</p>';
            };
        }
    });
})();
