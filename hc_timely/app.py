from aio_hipchat.hc_addon_app import HcAddonApp
from aio_hipchat.config_generator import ConfigGenerator

from hc_timely import timely_api

app_config = {
    "DEBUG": True,
    "ADDON_KEY": "simple-addon-example",
    "ADDON_NAME": "Simple Hipchat addon example",
    "ADDON_DESCRIPTION": "Playing with the framework to make it sing",
    "ADDON_VENDOR_NAME": "Atlassian",
    "ADDON_VENDOR_URL": "http://atlassian.com",
    "ADDON_FROM_NAME": "Phillip Piper",
    "ADDON_AVATAR": "http://objectlistview.sourceforge.net/cs/_static/faq-icon.png",
    "ADDON_AVATAR_2X": "http://objectlistview.sourceforge.net/cs/_static/faq-icon.png",
    "ADDON_ALLOW_ROOM": "true",
    "ADDON_ALLOW_GLOBAL": "false",
    "ADDON_SCOPES": "send_notification view_group view_room",  # space-separated list of scopes required by the add on

    "APP_NAME": "timely",
    "BASE_URL": "",
    "REDIS_URL": "redis://localhost:6379",
    "MONGO_URL": "mongodb://localhost:27017/test",
}


app = HcAddonApp(__file__, ConfigGenerator(app_config))
hc_api = timely_api.HcTimelyAddonApi(app)
