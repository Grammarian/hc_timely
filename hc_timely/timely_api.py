import asyncio
import aiohttp_jinja2

from aio_hipchat.hc_addon_api import HcAddonApi
from aio_hipchat.hc_structures import ImageCard, Glance, GlanceContainer, CardNotification, GlanceCondition
from aio_hipchat.log import make_default_logger
from aio_hipchat.pusher import MultiPusher
from aio_hipchat.hc_client import HcClient

from .timezone_controller import TimezoneController

GLANCE_KEY = 'timely.glance'
PANEL_KEY = 'egads-watson'

_logger = make_default_logger(__name__)


class HcTimelyAddonApi(HcAddonApi):
    """
    An instance of this class provides the actual implementation of the api for the webservice.
    """
    api = HcAddonApi

    def __init__(self, app):
        super().__init__(app)

        app.installed.subscribe(self.post_install)

        self.multi_server_pusher = MultiPusher.createSse(self.app, "/subscribe", ping_interval=10)

    @asyncio.coroutine
    def post_install(self, params):
        """
        This addon has just been installed. Let them know we are here
        """
        hc_client = HcClient(params['oauth'])
        _logger.debug("Post processing install into room %d", hc_client.oauth.room_id)

        ic = ImageCard(self.app.relative_to_base('/static/sunlightmap.png'),
                       side_by_side=True,
                       title="Welcome to Timely",
                       link_url="http://objectlistview.sourceforge.net",
                       description='The timely addon tracks users via timezone. '
                                   'Never send messages to colleagues at 2am every again')
        yield from hc_client.send_room_notification(CardNotification(ic))

    @api.message_hook('^/tz ', "tz-command")
    def handle_tz_webhook(self, request):
        hc_client = HcClient(request.oauth)

        c3 = ImageCard("http://www.skyscanner.net/static/sites/default/files/australia.jpg",
                       title="A nice title",
                       link_url="http://smh.com.au",
                       description=request.message.get('message')[3:],
                       image_url_2x="https://drscdn.500px.org/photo/116583489/m%3D2048/990100219838c055221df25c49b97b0d")

        yield from hc_client.send_room_notification(CardNotification(c3))

    @api.webhook('room_enter')
    def handle_room_enter(self, request):
        yield from self.update_glance(request)
        yield from self.update_webpanel(request)

    @api.webhook('room_exit')
    def handle_room_exit(self, request):
        yield from self.update_glance(request)
        yield from self.update_webpanel(request)

    @api.glance(GLANCE_KEY, "Timezones", target=PANEL_KEY,
                icon="/static/sunlightmap-icon.png", icon_hdpi="/static/sunlightmap-icon@2x.png",
                conditions=GlanceCondition("glance_matches", params={"attr": "timezone-count", "gt": 1}))
    def handle_glance(self, request):
        """
        This method is only called once(!) and the result is cached until an api message is sent to update it.
        """
        glance = yield from self.generate_glance(request.oauth)
        return glance

    @api.webpanel(PANEL_KEY, "Simple Data", icon="/static/simple.png")
    @aiohttp_jinja2.template('sidebar.jinja2')
    def handle_webpanel(self, request):
        """
        Return the html that will be included in the iframe of the sidebar.

        This method is only invoked when the sidebar is opened by a user.
        Staying up to date after that is the responsibility of the sidebar.
        """
        yield from self.update_webpanel(request)

        return {
            "signed_request": request.token,
            "base_url": request.app.base_url
        }

    @asyncio.coroutine
    def generate_glance(self, oauth):
        controller = TimezoneController(oauth)
        timezones = yield from controller.get_timezone_buckets()
        glance_label = "<b>{}</b> timezones".format(len(timezones))

        # Create a glance with some metadata. The metadata is related to the conditions that
        # were registered at the @api.glance call
        glance = Glance(glance_label, metadata={'timezone-count': len(timezones)})
        return glance

    @asyncio.coroutine
    def update_glance(self, request):
        glance = yield from self.generate_glance(request.oauth)
        hc_client = HcClient(request.oauth)
        yield from hc_client.push_room_glance(GlanceContainer({GLANCE_KEY: glance}))

    @asyncio.coroutine
    def update_webpanel(self, request):
        controller = TimezoneController(request.oauth)
        timezones = yield from controller.get_timezone_buckets()
        data = {'timezones': timezones}
        timezones_html = aiohttp_jinja2.render_string("timezones.jinja2", request, data)

        # We don't update the webpanel directly here. We push out the new html
        # to all web servers that are running and each one will forward the
        # new html to the clients that are connect to that server
        yield from self.multi_server_pusher.push({'html': timezones_html})



